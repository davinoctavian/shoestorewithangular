// src/app/user.service.ts
import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { of } from 'rxjs'

import * as shoesData from '../pages/api/db.json'

@Injectable({
    providedIn: 'root'
})

export class DataService {
    data = shoesData;

    constructor () {}

    getDatas (): Observable<any> {
        return of(this.data);
    }
}