import { Component, Input, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { Options, LabelType } from 'ngx-slider-v2';

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss']
})

export class SliderComponent implements OnInit, OnChanges {
  @Input() data: any;
  @Input() onchangeSlider: any;

  minValue: number = 100;
  maxValue: number = 1000;
  options: Options = {};
  roundedData: any[] = [];
  highlightData: any[] = [];

  ngOnInit(): void {
    const sortedData = this.data.slice().sort((a:any, b:any) => a - b);
    const roundData = [];
    for (var i = 0; i < sortedData.length; i++) {
        roundData[i] = Math.floor(sortedData[i]);
    }
    this.roundedData = roundData;
    this.highlightData = [roundData[0], roundData[roundData.length - 1]];
    this.minValue = roundData[0];
    this.maxValue = roundData[roundData.length - 1];
    this.options = {
      floor: 0,
      ceil: 1000,
      hideLimitLabels: true,
      translate: (value: number, label: LabelType): string => {
        switch (label) {
          case LabelType.Low:
            return "$" + this.minValue;
          case LabelType.High:
            return "$" + this.maxValue;
          default:
            return "$" + value;
        }
      }
    };
  }

  ngOnChanges(changes: SimpleChanges) {
    for (let property in changes) {
      if (property === 'data' && changes[property].previousValue) {
        if (changes[property].currentValue != changes[property].previousValue){
          const sortedData = changes[property].currentValue.slice().sort((a:any, b:any) => a - b);
          const roundData = [];
          for (var i = 0; i < sortedData.length; i++) {
            roundData[i] = Math.floor(sortedData[i]);
          }
          this.roundedData = roundData;
        }
      } 
    }
  }

  changeSlider = (event:any) => {
    if (event && event.value) {
      this.minValue = event.value;
      this.maxValue = event.highValue;
      this.highlightData = [event.value, event.highValue];
      this.onchangeSlider(this.highlightData);
    }
  }
}