import { Component, Input, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { Chart } from "chart.js/auto";

@Component({
  selector: 'app-bar-chart',
  templateUrl: './bar-chart.component.html',
  styleUrls: ['./bar-chart.component.scss']
})
export class BarChartComponent implements OnInit, OnChanges {
  @Input() data: any;
  @Input() highlight: any;

  public chart: any;
  dataUsed: any;
  highlightUsed: any;

  ngOnInit(): void {
    this.dataUsed = this.data;
    this.highlightUsed  = this.highlight;
    this.createChart();
  }
  
  ngOnChanges(changes: SimpleChanges) {
    for (let property in changes) {
      if ((property === 'data' || property === 'highlight') && changes[property].previousValue) {
        if (changes[property].currentValue != changes[property].previousValue){
          if (property === 'data') {
            this.dataUsed = changes['data'].currentValue;
          }
          if (property === 'highlight') {
            this.highlightUsed = changes['highlight'].currentValue;
          }

          let counts:any = {};
          for (var i = 0; i < this.dataUsed.length; i++) {
            counts[this.dataUsed[i]] = counts[this.dataUsed[i]] + 1 || 1;
          }
          // generate data
          const barDataValues = [];
          for (let i = 0; i < 1000; i++) {
            barDataValues.push(counts[i] || 0);
          }
          
          const barData = {
            labels: barDataValues.map((val, i) => i),
            datasets: [
                {
                    backgroundColor: barDataValues.map((val, i) =>
                        i >= this.highlightUsed[0] && i <= this.highlightUsed[1]
                        ? "rgba(135, 206, 235, 1)"
                        : "rgba(255, 99, 132, 0.2)"
                    ),
                    hoverBackgroundColor: "rgba(255,99,132,0.4)",
                    data: barDataValues
                }
            ]
          };
          
          this.chart.data = barData;
          this.chart.update();
        }
      }
    }
  }

  createChart(){
    let counts:any = {};
    for (var i = 0; i < this.dataUsed.length; i++) {
      counts[this.dataUsed[i]] = counts[this.dataUsed[i]] + 1 || 1;
    }
    // generate data
    const barDataValues = [];
    for (let i = 0; i < 1000; i++) {
      barDataValues.push(counts[i] || 0);
    }
    
    const barData = {
      labels: barDataValues.map((val, i) => i),
      datasets: [
          {
              backgroundColor: barDataValues.map((val, i) =>
                  i >= this.highlightUsed[0] && i <= this.highlightUsed[1]
                  ? "rgba(135, 206, 235, 1)"
                  : "rgba(255, 99, 132, 0.2)"
              ),
              hoverBackgroundColor: "rgba(255,99,132,0.4)",
              data: barDataValues
          }
      ]
    };

    this.chart = new Chart("MyChart", {
      type: 'bar',
      data: barData,
      options: {
        responsive: true,
        maintainAspectRatio: false,
        plugins: {
            legend: {
                display: false
            },
            tooltip: {
              enabled: false
            }
        },
        scales: {
          y: {
            display: false
          },
          x: {
            display: false
          }
        }
      }
      
    });
  }
}