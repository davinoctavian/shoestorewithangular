import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NgxSliderModule } from 'ngx-slider-v2';
import { SliderComponent, BarChartComponent } from './';
 
 @NgModule({
  imports: [
    CommonModule,
    NgxSliderModule
  ],
  declarations: [ SliderComponent, BarChartComponent ],
  exports: [
    CommonModule,
    SliderComponent,
    BarChartComponent
  ]
})
export class ComponentModule {
}