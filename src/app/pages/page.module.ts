import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { ComponentModule } from './../components/component.module';
import { HomeComponent } from './';
 
 @NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    RouterModule,
    ComponentModule
  ],
  declarations: [ HomeComponent ],
  exports: [
    CommonModule,
    HttpClientModule,
    RouterModule,
    HomeComponent
  ]
})
export class PageModule {
}