import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})

export class HomeComponent implements OnInit {
    shoes:any = null;
    name = '';
    categories:any = null;
    size:any = null;
    prices:any = [100, 1000];
    sliderPrice:any = null;
    sortText = "Price";
    checkedStateCategory:any = null;
    checkedStateSize:any = null;
    filteredData:any = null;
    isSliderChange: boolean = false;
    isCategoryMenuOpened: boolean = true;

    constructor(private dataService: DataService) { }

    ngOnInit(): void {
        this.dataService.getDatas().subscribe(data => {
            this.shoes = data.shoes;
            this.filteredData = data.shoes;
        });
        
        //for shoes change
        if (this.shoes && this.shoes != null) {
            const shoesSize = this.shoes.map((a:any) => a.size);
            const removeDuplicateSize = [...new Set(shoesSize)];
            const sortShoesSize = removeDuplicateSize.sort();
    
            const shoesCategories = this.shoes.map((a:any) => a.category);
            const removeDuplicateCategory = [...new Set(shoesCategories)];
    
            const shoesPrice = this.shoes.map((a:any) => a.price);
            const removeDuplicatePrice = [...new Set(shoesPrice)];
    
            this.size = sortShoesSize;
            this.categories = removeDuplicateCategory;
            this.prices = removeDuplicatePrice;
    
            this.checkedStateCategory = new Array(removeDuplicateCategory.length).fill(false);
            this.checkedStateSize = new Array(sortShoesSize.length).fill(false);
    
            const filterData = this.sortByKey(this.filteredData, this.sortText.toLowerCase());
            this.filteredData = filterData;
        }
        this.isSliderChange = false;
    }

    handleCategoryClick = () => {
        this.isCategoryMenuOpened = !this.isCategoryMenuOpened;
    }

    handleOnChangeSearch = (e:any) => {
        this.name = e.target.value;
        this.isSliderChange = false;
        this.handleAllChange();
    }

    handleSortClick = (e:any) => {
        const valueSort = e.target.innerHTML;
        this.sortText = valueSort;
        this.isSliderChange = false;
        const filterData = this.sortByKey(this.filteredData, valueSort.toLowerCase());
        this.filteredData = filterData;
    }

    sortByKey = (arr:any, key:any) => {
        return arr.sort(function(a:any, b:any) {
            var x = a[key]; var y = b[key];
            return ((x < y) ? -1 : ((x > y) ? 1 : 0));
        })
    }

    handleOnChangeCategory = (position:any) => {
        const updatedCheckedState = this.checkedStateCategory.map((item:any, index:number) =>
          index === position ? !item : item
        )
        this.checkedStateCategory = updatedCheckedState;
        this.isSliderChange = false;
        this.handleAllChange();
    }

    handleOnChangeSize = (position:any) => {
        const updatedCheckedState = this.checkedStateSize.map((item:any, index:number) =>
            index === position ? !item : item
        )
        this.checkedStateSize = updatedCheckedState;
        this.isSliderChange = false;
        this.handleAllChange();
    }

    onChangeSlider = (data:any) => {
        if (data && isNaN(data[0])) {
            this.sliderPrice = null;
        }
        else {
            this.sliderPrice = data;
        }
        this.isSliderChange = true;
        this.handleAllChange();
    }

    handleAllChange = () => {
        if (this.categories && this.size) {
            const filterCategory = this.categories.filter((item:any, index:number) => this.checkedStateCategory[index] === true);
            const filterDataCategory = this.shoes.filter((item:any) => {
                if (filterCategory && filterCategory.length) {
                    return filterCategory.includes(item.category);
                }
                return true;
            })

            const filterSize = this.size.filter((item:any, index:number) => this.checkedStateSize[index] === true);
            const filterDataSize = filterDataCategory.filter((item:any) => {
                if (filterSize && filterSize.length) {
                    return filterSize.includes(item.size);
                }
                return true;
            })

            const filterDataName = filterDataSize.filter((item:any) => {
                if (this.name && this.name != '') {
                    let nameLowercase = this.name.toString().toLowerCase();
                    return item.name.toString().toLowerCase().includes(nameLowercase);
                }
                return true;
            })

            const sortData = this.sortByKey(filterDataName, this.sortText.toLowerCase());

            const filterDataPrice = sortData.filter((item:any) => {
                if(this.sliderPrice) {
                    return item.price >= this.sliderPrice[0] && item.price <= this.sliderPrice[1];
                }
                return true;
            })

            if (this.isSliderChange == false) {
                const shoesPrice = sortData.map((a:any) => a.price);
                const removeDuplicatePrice = [...new Set(shoesPrice)];
                this.prices = removeDuplicatePrice;
            }

            this.filteredData = filterDataPrice;
        }
    }
}