# ShoeStoreAngular

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 16.0.1.
with npm version 8.19.3 and node version 18.13.0

## Information
after clone this project make sure it have node_modules then try npm start and if failed you need to to this step:
1. npm install
2. npm start

note: if npm install failed do this:
npm install --save ngx-slider-v2 --force
maybe because its running different version of angular in ngx-slider-v2, this project not use ngx-slider because its no longer maintain.

[Video of Result](https://gitlab.com/davinoctavian/shoestorewithangular/-/blob/main/src/assets/ShoeStoreAngularTypescript.mp4)
